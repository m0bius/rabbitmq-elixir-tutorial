# Topics

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed as:

  1. Add topics to your list of dependencies in `mix.exs`:

        def deps do
          [{:topics, "~> 0.0.1"}]
        end

  2. Ensure topics is started before your application:

        def application do
          [applications: [:topics]]
        end

