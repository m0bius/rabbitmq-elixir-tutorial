{:ok, connection} = AMQP.Connection.open("amqp://guest:guest@rabbitmq")
{:ok, channel} = AMQP.Channel.open(connection)

{topic, message} =
  System.argv
  |> case do
    []            -> {"anonymous.info", "Hello World!"}
    [message]         -> {"anonymous.info", message}
    [topic|words] -> {topic, Enum.join(words, " ")}
  end


AMQP.Exchange.declare(channel, "topic_log", :topic)

AMQP.Basic.publish(channel, "topic_log", topic, message)
IO.puts" [x] Send  '[#{topic}] #{message}'"

AMQP.Connection.close(connection)
