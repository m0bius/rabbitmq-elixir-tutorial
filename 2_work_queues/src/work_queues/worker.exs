defmodule Worker do
    def wait_for_message(channel) do
      receive do
        {:basic_deliver, payload, meta} ->
          IO.puts " [x] Received '#{payload}'"
          payload
          |> to_char_list
          |> Enum.count(fn x -> x == ?. end)
          |> Kernel.*(1000)
          |> :timer.sleep
          IO.puts  " [x] Done"
          AMQP.Basic.ack(channel, meta.delivery_tag)
          
          wait_for_message(channel)
      end
    end
end

{:ok, connection} = AMQP.Connection.open("amqp://guest:guest@rabbitmq")
{:ok, channel} = AMQP.Channel.open(connection)
AMQP.Queue.declare(channel, "task_queue", durable: true)
AMQP.Basic.qos(channel, prefetch_count: 1)

AMQP.Basic.consume(channel, "task_queue")
IO.puts " [y] Waiting for messages, to exit press 2 times CTRL+C"

Worker.wait_for_message(channel)
