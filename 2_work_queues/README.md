Questa configurazione di container permette di avere 3 services istanziati assieme:  
1- elixir: è il runtime di Elixir che conterrà anche i file del progetto  
2- data: è un container passivo, nel senso che viene usato come volume da elixir  
3- rabbitmq: è il servizio che gestisce i messaggi  

Si può notare dal file docker-compose che elixir è stato collegato a rabbitmq per poterlo usare come server.  
Quindi nell'esempio si vuole ottenere una cosa simile:  
elixir <=> rabbitmq <=> elixir  

Nell'esempio utilizziamo l'option "scale" per scalare a 2 istanze diverse del container Elixir

## Istruzioni per costruire le immagini, creare ed avviare i container

Prima bisogna settare una variable che viene usata per montare il volume,
mettere come valore della variabile il path assoluto alla cartella che volete
montare nel container  
```
export MW_VOLUME=path_alla_cartella_con_i_dati
```
buildiamo le immagini
```
docker-compose build
```
dopo aver fatto ciò possiamo istanziare ed eseguire i containers  
```
docker-compose up
```
ora abbiamo i 2 container in esecuzione, quello relativo ad Elixir e quello relativo a RabbitMQ.  
Vogliamo però aggiungere un'altra istanza del container Elixir alle istanze in esecuzione
```
docker scale elixir=2
```
In esecuzione:  
* rabbitmq
* elixir1 // istanza 1  
* elixir2 // istanza 2  

Ora apriamo 3 sessioni diverse:  
1. elixir1 -> server
2. elixir1 -> server
3. elixir2 -> client

Utilizzare il seguente comando per aprire una sessione interna al container elixir
```
docker exec -it -u cds elixir bash
```
In una qualsiasi delle sessioni aperte implementiamo l'esempio 2 delle work_queues, il volume che tutti i container Elixir utilizzano è lo stesso e quindi i sorgenti sono condivisi dalle 2 istanze.  
Una volta completato l'esempio eseguiamo nelle 2 sessioni elixir1 precedentemente aperte  
```
mix run worker.exs
```
Così abbiamo 2 workers pronti a ricevere i messaggi che verrano creati.  
In elixir2 creiamo n messaggi  
```
mix run new_task.exs 'N ......'
```
tante volte quanti sono i messaggi che vogliamo inviare.   
Possiamo vedere quale sessione di elixir1 processa i messaggi  
Potete anche controllare i log di rabbitmq dall'altro terminale in cui 
sta girando il server



