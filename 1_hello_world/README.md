Questa configurazione di container permette di avere 3 services istanziati 
assieme:  
1- elixir: è il runtime che conterrà anche i file del progetto  
2- data: è un container passivo, nel senso che viene usato come volume da elixir.
L'utilità di questo container sta nel fatto che permette di montare una cartella 
locale della nostra macchina direttamente nel container in modo da rendere 
possibile scrivere il proprio codice con l'editor che si preferisce e questo
è condiviso all'interno del container che vede i file scritti  
3- rabbitmq: è il servizio che gestisce i messaggi  

Si può notare dal file docker-compose che elixir è stato collegato a
rabbitmq per poterlo usare come server. Quindi nell'esempio si vuole ottenere  
una cosa simile:  
elixir <=> rabbitmq <=> elixir  
Ora per semplicità usiamo 1 solo container Elixir e 1 solo container RabbitMQ  
ma simuliamo il fatto di avere 2 container Elixir aprendo 2 sessioni diverse
nello stesso container
## Istruzioni per far andare il tutto  

Prima bisogna settare una variable che viene usata per montare il volume,
mettere come valore della variabile il path assoluto alla cartella che volete
montare nel container  
```
export MW_VOLUME=path_alla_cartella_con_i_dati
```
dopodichè buildiamo le immagini
```
docker-compose build
```
dopo aver fatto ciò possiamo istanziare ed eseguire i containers  
```
docker-compose up
```
ora abbiamo i 2 container che ci servono in esecuzione. Apriamo 
altri 2 terminali con docker, per loggarci 2 volte nello stesso
container e simulare la send receive.
Per loggarci nel container Elixir  
```
docker exec -it -u cds elixir bash
```
Nell'altro terminale aperto logghiamoci sempre con lo stesso comando di prima.  
Ora abbiamo 2 sessioni diverse sulla stesso container entrambe collegate a
RabbitMQ, dalla cartella che abbiamo montato possiamo scrivere i sorgenti del
primo tutorial HelloWorld e modificare le dipendenze di mix. Dopo aver fatto ciò
eseguiamo nella prima sessione di Elixir
```
mix run send.exs
```
Nella seconda 
```
mix run receive.exs
```
Potete anche controllare i log di rabbitmq dall'altro terminale in cui 
sta girando il server e vedere che effettivamente i messagi sono tracciati 
dal server  



