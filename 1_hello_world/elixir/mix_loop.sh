#!/bin/bash
# install project dependencies using Hex (git not enabled)
cd /home/cds
if [ ! -d hello_world ]; then
  mix new hello_world
fi
cd hello_world
mix deps.get
mix deps.compile
# infinite loop to keep the container runnig
echo "Press [CTRL+C] to stop ..."
while true;
do
	sleep 24h
done
